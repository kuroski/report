import store from 'store';
import _ from 'lodash';
import db from '../db';
import api from '../api';

export default {
  FETCH_ISSUES: async ({ commit, state }) => {
    const issuesTable = await db.issues.get(state.teamLabel);
    const startingIssues = (issuesTable && issuesTable.data) || [];
    commit('SET_LOADING', true);
    const newIssues = await api.fetchAllTeamIssues(state.teamLabel, _.last(startingIssues));
    const issues = _.unionBy(startingIssues, newIssues, issue => issue.id);
    await db.issues.put({
      team: state.teamLabel,
      data: issues
    });
    commit('SET_ISSUES', issues);
    commit('SET_LOADING', false);
  },

  FETCH_LABELS: async ({ commit, dispatch }) => {
    commit('SET_LOADING', true);
    let labels = store.get('labels') || [];
    let selectedLabel = store.get('selectedLabel') || 'equipe:ExD-Team';
    if (!labels.length) {
      labels = await api.fetchLabels();
      store.set('labels', labels);
    }
    dispatch('CHANGE_TEAM_LABEL', selectedLabel);
    commit('SET_LABELS', labels);
    commit('SET_LOADING', false);
  },

  CHANGE_TEAM_LABEL: async ({ commit, dispatch }, teamLabel) => {
    store.set('selectedLabel', teamLabel);
    commit('SET_TEAM_LABEL', teamLabel);
    dispatch('FETCH_ISSUES');
  }
};
