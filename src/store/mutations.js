import loadingService from '../services/loading';

export default {
  SET_ISSUES: (state, issues) => {
    state.issues = issues
  },

  SET_LABELS: (state, labels) => {
    state.labels = labels
  },

  SET_TEAM_LABEL: (state, value) => {
    state.teamLabel = value;
  },

  SET_LOADING: (state, isLoading) => {
    if (isLoading) loadingService.loadingAgents += 1;
    else loadingService.loadingAgents -= 1;

    if (!isLoading && loadingService.loadingAgents) return;
    state.loading = isLoading;
  },
};
