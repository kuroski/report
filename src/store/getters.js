const { DateTime } = require('luxon');
const _ = require('lodash');

const filterIssueByLabels = labels => issue => issue.labels
  .filter(label => labels.includes(label.name)).length;

export default {
  availableIssues: (state) => (labels) => {
    let issues = state.issues.filter(issue => !issue.labels.find(label => label.name === 'IGNORE'));

    if (labels && labels.length)
      issues = issues.filter(filterIssueByLabels(labels));

    return issues;
  },

  issuesZendeskClosedByMonth: (state) => () => {
    const issues = state
      .issues
      .filter(issue => !issue.labels.find(label => label.name === 'IGNORE'))
      .filter(filterIssueByLabels(['ZENDESK']))
      .filter(issue => issue.closed_at !== null)
      const test = _.groupBy(issues, (issue) => {
        return DateTime.fromISO(issue.closed_at).toFormat('LLL yy');
      })
      console.log(test)
    debugger
    return _.groupBy(issues, (issue) => {
      return DateTime.fromISO(issue.closed_at).toFormat('LLL yy');
    });
  },

  issuesByMonth: (state) => (labels) => {
    const issues = state.issues.filter(issue => !issue.labels.find(label => label.name === 'IGNORE'));
    // TODO: Add funcionality to filter by date
    // issues = issues.filter(issue => DateTime.fromISO(issue.created_at) >= DateTime.fromISO('2018-01-01'));
    let issuesGroup = _.groupBy(issues, (issue) => {
      const createdAt = DateTime.fromISO(issue.created_at);
      return createdAt.toFormat('LLL yy');
    });

    if (labels && labels.length)
      Object
        .keys(issuesGroup)
        .forEach((group) => {
          issuesGroup[group] = issuesGroup[group].filter(filterIssueByLabels(labels));
        });

    return issuesGroup;
  },

  throughputByMonth: (state) => () => {
    const issues = state.issues.filter(issue => (issue.state === 'closed' && !issue.labels.find(label => label.name === 'IGNORE')));
    // TODO: Add funcionality to filter by date
    // issues = issues.filter(issue => DateTime.fromISO(issue.created_at) >= DateTime.fromISO('2018-01-01'));
    let issuesGroup = _.groupBy(issues, (issue) => {
      const createdAt = DateTime.fromISO(issue.created_at);
      return createdAt.toFormat('LLL yy');
    });

    return issuesGroup;
  },

  teamLabels: (state) => () => {
    return state.labels.filter(label => label.name.includes('equipe:')).map(filter => filter.name);
  }
};
