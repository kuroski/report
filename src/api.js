const octokit = require('@octokit/rest')({
  timeout: 200,
});
const { DateTime } = require('luxon');
const config = {
  owner: 'involvestecnologia',
  repo: 'agilepromoterissues',
  per_page: 100,
}

octokit.authenticate({
  type: 'token',
  token: process.env.VUE_APP_GITHUB_TOKEN || 'provide-a-token',
});

export default {
  fetchLabels: async function() {
    const res = await octokit.issues.getLabels({...config});

    return res.data;
  },

  fetchAllTeamIssues: async function(teamLabel, lastIssueCreated) {
    const issues = [];
    const searchConfig = {
      ...config,
      state: 'all',
      labels: teamLabel,
      direction: 'asc',
    };

    if (lastIssueCreated)
      searchConfig.since = DateTime
        .fromISO(lastIssueCreated.created_at)
        .toFormat('yyyy-MM-dd HH:mm:ss');

    const paginate = async (res) => {
      const next = await octokit.getNextPage(res);
      issues.push(...next.data);

      if (octokit.hasNextPage(next)) await paginate(next);
    };

    const res = await octokit.issues.getForRepo(searchConfig);

    issues.push(...res.data);

    if (octokit.hasNextPage(res)) await paginate(res);

    return issues;
  },
};
