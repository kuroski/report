import Vue from 'vue'
import Router from 'vue-router'
const ReportView = () => import('../views/ReportView')

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'ReportView',
      component: ReportView
    }
  ]
})
