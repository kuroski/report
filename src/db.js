import Dexie from 'dexie';

const db = new Dexie('Report');
db.version(2).stores({ issues: 'team, data' });

export default db;
