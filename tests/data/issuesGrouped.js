export default {
  '2016_11': [
    {
      "url": "https://api.github.com/repos/involvestecnologia/agilepromoterissues/issues/2775",
      "repository_url": "https://api.github.com/repos/involvestecnologia/agilepromoterissues",
      "labels_url": "https://api.github.com/repos/involvestecnologia/agilepromoterissues/issues/2775/labels{/name}",
      "comments_url": "https://api.github.com/repos/involvestecnologia/agilepromoterissues/issues/2775/comments",
      "events_url": "https://api.github.com/repos/involvestecnologia/agilepromoterissues/issues/2775/events",
      "html_url": "https://github.com/involvestecnologia/agilepromoterissues/issues/2775",
      "id": 278452527,
      "number": 2775,
      "title": "Erro ao tentar inserir um PDV no roteiro de um colaborador através de um usuário com Perfil de Stakeholder",
      "user": {
        "login": "gabrielinvolves",
        "id": 17481937,
        "avatar_url": "https://avatars1.githubusercontent.com/u/17481937?v=4",
        "gravatar_id": "",
        "url": "https://api.github.com/users/gabrielinvolves",
        "html_url": "https://github.com/gabrielinvolves",
        "followers_url": "https://api.github.com/users/gabrielinvolves/followers",
        "following_url": "https://api.github.com/users/gabrielinvolves/following{/other_user}",
        "gists_url": "https://api.github.com/users/gabrielinvolves/gists{/gist_id}",
        "starred_url": "https://api.github.com/users/gabrielinvolves/starred{/owner}{/repo}",
        "subscriptions_url": "https://api.github.com/users/gabrielinvolves/subscriptions",
        "organizations_url": "https://api.github.com/users/gabrielinvolves/orgs",
        "repos_url": "https://api.github.com/users/gabrielinvolves/repos",
        "events_url": "https://api.github.com/users/gabrielinvolves/events{/privacy}",
        "received_events_url": "https://api.github.com/users/gabrielinvolves/received_events",
        "type": "User",
        "site_admin": false
      },
      "labels": [
        {
          "id": 456432297,
          "url": "https://api.github.com/repos/involvestecnologia/agilepromoterissues/labels/ZENDESK",
          "name": "ZENDESK",
          "color": "FF0000",
          "default": false
        },
        {
          "id": 598244498,
          "url": "https://api.github.com/repos/involvestecnologia/agilepromoterissues/labels/equipe:ExD-Team",
          "name": "equipe:ExD-Team",
          "color": "fef2c0",
          "default": false
        },
        {
          "id": 456431429,
          "url": "https://api.github.com/repos/involvestecnologia/agilepromoterissues/labels/motivo:defeito",
          "name": "motivo:defeito",
          "color": "FF0000",
          "default": false
        },
        {
          "id": 657168539,
          "url": "https://api.github.com/repos/involvestecnologia/agilepromoterissues/labels/prioridade:media",
          "name": "prioridade:media",
          "color": "e6e600",
          "default": false
        },
        {
          "id": 456381530,
          "url": "https://api.github.com/repos/involvestecnologia/agilepromoterissues/labels/time:web",
          "name": "time:web",
          "color": "e6e6e6",
          "default": false
        }
      ],
      "state": "closed",
      "locked": false,
      "assignee": null,
      "assignees": [

      ],
      "milestone": null,
      "comments": 36,
      "created_at": "2017-12-01T12:31:23Z",
      "updated_at": "2018-03-06T22:29:03Z",
      "closed_at": "2018-02-23T16:24:58Z",
      "author_association": "OWNER",
      "body": "This Github issue is synchronized with Zendesk:\n\n**Ticket ID:** [#27681](https://agilepromoter.zendesk.com/agent/tickets/27681)\n**Priority:** normal\n**Group:** System Never Down\n**Requester:** alex.naves@manfrim.com.br\n**Organization:** Special Dog\n**Assignee:** Alvaro Coelho\n**Issue escalated by:** Alvaro Coelho\n**Produto:** produto_agile_web\n**Tipo:** comportamento_inesperado\n**Url do Sistema:** https://specialdog.agilepromoter.com/webapp/#!/app/Mflfx4vR~2FUIfTPLg5S4O8Q==/roteiros/43/avulso\n**Ambiente:** special dog\n**Versão Web do Cliente:** 2.17.07.13\n**Versão Mobile do Cliente:** 2.17.07.1\n\n\n**Original ticket description:**\n\n<blockquote><div class=\"zd-comment\"><p dir=\"auto\">Ao editar roteiro no Stakeholder Aline Scarpin, aparece a seguinte mensagem:\n<br>Ocorreu um erro inesperado no sistema, contate a equipe de suporte!</p></div></blockquote>\n\n\n#### Attachments\n\n* [Aline Maringa_Roteiro.png](https://agilepromoter.zendesk.com/attachments/token/PBm9eE2lyuBbcteFhomNaKuZx/?name=Aline+Maringa_Roteiro.png)\n\n\n",
      "reactions": {
        "url": "https://api.github.com/repos/involvestecnologia/agilepromoterissues/issues/2775/reactions",
        "total_count": 0,
        "+1": 0,
        "-1": 0,
        "laugh": 0,
        "hooray": 0,
        "confused": 0,
        "heart": 0
      }
    },
    {
      "url": "https://api.github.com/repos/involvestecnologia/agilepromoterissues/issues/3253",
      "repository_url": "https://api.github.com/repos/involvestecnologia/agilepromoterissues",
      "labels_url": "https://api.github.com/repos/involvestecnologia/agilepromoterissues/issues/3253/labels{/name}",
      "comments_url": "https://api.github.com/repos/involvestecnologia/agilepromoterissues/issues/3253/comments",
      "events_url": "https://api.github.com/repos/involvestecnologia/agilepromoterissues/issues/3253/events",
      "html_url": "https://github.com/involvestecnologia/agilepromoterissues/issues/3253",
      "id": 298565137,
      "number": 3253,
      "title": "Testes de Integração para o endpoint de alteração de status de Ponto de Venda",
      "user": {
        "login": "icarneiro",
        "id": 11747824,
        "avatar_url": "https://avatars3.githubusercontent.com/u/11747824?v=4",
        "gravatar_id": "",
        "url": "https://api.github.com/users/icarneiro",
        "html_url": "https://github.com/icarneiro",
        "followers_url": "https://api.github.com/users/icarneiro/followers",
        "following_url": "https://api.github.com/users/icarneiro/following{/other_user}",
        "gists_url": "https://api.github.com/users/icarneiro/gists{/gist_id}",
        "starred_url": "https://api.github.com/users/icarneiro/starred{/owner}{/repo}",
        "subscriptions_url": "https://api.github.com/users/icarneiro/subscriptions",
        "organizations_url": "https://api.github.com/users/icarneiro/orgs",
        "repos_url": "https://api.github.com/users/icarneiro/repos",
        "events_url": "https://api.github.com/users/icarneiro/events{/privacy}",
        "received_events_url": "https://api.github.com/users/icarneiro/received_events",
        "type": "User",
        "site_admin": false
      },
      "labels": [
        {
          "id": 469209909,
          "url": "https://api.github.com/repos/involvestecnologia/agilepromoterissues/labels/API",
          "name": "API",
          "color": "5319e7",
          "default": false
        },
        {
          "id": 598244498,
          "url": "https://api.github.com/repos/involvestecnologia/agilepromoterissues/labels/equipe:ExD-Team",
          "name": "equipe:ExD-Team",
          "color": "fef2c0",
          "default": false
        },
        {
          "id": 456431572,
          "url": "https://api.github.com/repos/involvestecnologia/agilepromoterissues/labels/motivo:melhoria",
          "name": "motivo:melhoria",
          "color": "33ccff",
          "default": false
        },
        {
          "id": 657168539,
          "url": "https://api.github.com/repos/involvestecnologia/agilepromoterissues/labels/prioridade:media",
          "name": "prioridade:media",
          "color": "e6e600",
          "default": false
        },
        {
          "id": 456381530,
          "url": "https://api.github.com/repos/involvestecnologia/agilepromoterissues/labels/time:web",
          "name": "time:web",
          "color": "e6e6e6",
          "default": false
        }
      ],
      "state": "open",
      "locked": false,
      "assignee": null,
      "assignees": [

      ],
      "milestone": null,
      "comments": 0,
      "created_at": "2018-02-20T11:48:10Z",
      "updated_at": "2018-03-06T18:16:09Z",
      "closed_at": null,
      "author_association": "CONTRIBUTOR",
      "body": "## Contexto:\r\nAssim como aplicado nos demais endpoints, será necessário a implementação de novos testes de integração.\r\n\r\n## Objetivo\r\nImplementar os testes de integração referente à issue #2527",
      "reactions": {
        "url": "https://api.github.com/repos/involvestecnologia/agilepromoterissues/issues/3253/reactions",
        "total_count": 0,
        "+1": 0,
        "-1": 0,
        "laugh": 0,
        "hooray": 0,
        "confused": 0,
        "heart": 0
      }
    },
  ],
  '2016_12': [
    {
      "url": "https://api.github.com/repos/involvestecnologia/agilepromoterissues/issues/3276",
      "repository_url": "https://api.github.com/repos/involvestecnologia/agilepromoterissues",
      "labels_url": "https://api.github.com/repos/involvestecnologia/agilepromoterissues/issues/3276/labels{/name}",
      "comments_url": "https://api.github.com/repos/involvestecnologia/agilepromoterissues/issues/3276/comments",
      "events_url": "https://api.github.com/repos/involvestecnologia/agilepromoterissues/issues/3276/events",
      "html_url": "https://github.com/involvestecnologia/agilepromoterissues/issues/3276",
      "id": 298726298,
      "number": 3276,
      "title": "[OKR] Comportamento inesperado nos Filtros novos ",
      "user": {
        "login": "mpossel",
        "id": 6353758,
        "avatar_url": "https://avatars3.githubusercontent.com/u/6353758?v=4",
        "gravatar_id": "",
        "url": "https://api.github.com/users/mpossel",
        "html_url": "https://github.com/mpossel",
        "followers_url": "https://api.github.com/users/mpossel/followers",
        "following_url": "https://api.github.com/users/mpossel/following{/other_user}",
        "gists_url": "https://api.github.com/users/mpossel/gists{/gist_id}",
        "starred_url": "https://api.github.com/users/mpossel/starred{/owner}{/repo}",
        "subscriptions_url": "https://api.github.com/users/mpossel/subscriptions",
        "organizations_url": "https://api.github.com/users/mpossel/orgs",
        "repos_url": "https://api.github.com/users/mpossel/repos",
        "events_url": "https://api.github.com/users/mpossel/events{/privacy}",
        "received_events_url": "https://api.github.com/users/mpossel/received_events",
        "type": "User",
        "site_admin": false
      },
      "labels": [
        {
          "id": 598244498,
          "url": "https://api.github.com/repos/involvestecnologia/agilepromoterissues/labels/equipe:ExD-Team",
          "name": "equipe:ExD-Team",
          "color": "fef2c0",
          "default": false
        },
        {
          "id": 456431429,
          "url": "https://api.github.com/repos/involvestecnologia/agilepromoterissues/labels/motivo:defeito",
          "name": "motivo:defeito",
          "color": "FF0000",
          "default": false
        },
        {
          "id": 657175708,
          "url": "https://api.github.com/repos/involvestecnologia/agilepromoterissues/labels/prioridade:critico",
          "name": "prioridade:critico",
          "color": "ff6600",
          "default": false
        },
        {
          "id": 665211300,
          "url": "https://api.github.com/repos/involvestecnologia/agilepromoterissues/labels/status:feedback",
          "name": "status:feedback",
          "color": "d93f0b",
          "default": false
        },
        {
          "id": 456381530,
          "url": "https://api.github.com/repos/involvestecnologia/agilepromoterissues/labels/time:web",
          "name": "time:web",
          "color": "e6e6e6",
          "default": false
        }
      ],
      "state": "closed",
      "locked": false,
      "assignee": null,
      "assignees": [

      ],
      "milestone": null,
      "comments": 3,
      "created_at": "2018-02-20T19:52:48Z",
      "updated_at": "2018-03-06T11:32:16Z",
      "closed_at": "2018-03-06T11:23:51Z",
      "author_association": "CONTRIBUTOR",
      "body": "## Comportamento atual 1:\r\n**Na tela de Afastamento, quando o usuário tem um filtro padrão salvo e configura um novo filtro sem salvar. Clica em pesquisar e clica para visualizar um resultado, ao voltar, o filtro pesquisado nao aparece mais, mas sim o filtro padrão salvo. \r\n\r\n## Comportamento atual 2 (corrigido na tela de Painel de Pesquisas):\r\n** Na tela de Afastamento, quando o usuário configura um filtro, salvando ou não, quando clica para editar um resultado e volta para a listagem, o filtro não eh mantido, volta para o default ou um filtro padrão salvo. \r\n\r\n## Comportamento atual 3: \r\n** Na tela de Dashboard (analisar também se em outras telas com \"filtros padrões/rotas\"), ao clicar no link Tarefas do dia e se tiver um filtro salvo como padrão, irá abrir esta tela com este filtro padrão salvo e nao o filtro do link. Porém o resultado é o do link. \r\n\r\n## Comportamento atual 4: \r\n** Busca dos filtros dinâmicos não filtra palavras com acento. Ex: Filtrar por \"formulario\", aparece. Já por \"formulário\" filtro não é encontrado.\r\n\r\nOBS: Comportamento do mulselect \"padrão\" é ao contrário.\r\n\r\n## Comportamento atual 5: \r\n** Ao setar como `false` a propriedade `enableSaveFilters` o botão de salvar filtros continua aparecendo na tela.\r\n\r\n## Possível solução:\r\n#### Possível solução comportamento 2:\r\n\r\nNa tela \"Painel de pesquisas\" existem uma solução implementada para salvar os ultimos filtros utilizados na pesquisa, salvando o **availableFilters** no **$invViewUtils.lastSearchFilters**\r\n\r\n```\r\n$invViewUtils.lastSearchFilters({\r\n                availableFilters: $scope.filtersConfig.availableFilters,\r\n                filtersStatus: $scope.filtersStatus,\r\n                arrFilterStatus: $scope.arrFilterStatus,\r\n                buttonsFilterStatus: $scope.buttonsFilterStatus,\r\n                pagingOptions,\r\n                sortOptions,\r\n              });\r\n```\r\nAssim o controller sabe também quais os campos devem ser exibidor e atualizados na view e não apenas seus valores.\r\n\r\n## Ambiente:\r\n* Ambiente: homologacao \r\n",
      "reactions": {
        "url": "https://api.github.com/repos/involvestecnologia/agilepromoterissues/issues/3276/reactions",
        "total_count": 0,
        "+1": 0,
        "-1": 0,
        "laugh": 0,
        "hooray": 0,
        "confused": 0,
        "heart": 0
      }
    },
    {
      "url": "https://api.github.com/repos/involvestecnologia/agilepromoterissues/issues/3356",
      "repository_url": "https://api.github.com/repos/involvestecnologia/agilepromoterissues",
      "labels_url": "https://api.github.com/repos/involvestecnologia/agilepromoterissues/issues/3356/labels{/name}",
      "comments_url": "https://api.github.com/repos/involvestecnologia/agilepromoterissues/issues/3356/comments",
      "events_url": "https://api.github.com/repos/involvestecnologia/agilepromoterissues/issues/3356/events",
      "html_url": "https://github.com/involvestecnologia/agilepromoterissues/issues/3356",
      "id": 302449125,
      "number": 3356,
      "title": "Wahl - Limpar mix!",
      "user": {
        "login": "gabrielinvolves",
        "id": 17481937,
        "avatar_url": "https://avatars1.githubusercontent.com/u/17481937?v=4",
        "gravatar_id": "",
        "url": "https://api.github.com/users/gabrielinvolves",
        "html_url": "https://github.com/gabrielinvolves",
        "followers_url": "https://api.github.com/users/gabrielinvolves/followers",
        "following_url": "https://api.github.com/users/gabrielinvolves/following{/other_user}",
        "gists_url": "https://api.github.com/users/gabrielinvolves/gists{/gist_id}",
        "starred_url": "https://api.github.com/users/gabrielinvolves/starred{/owner}{/repo}",
        "subscriptions_url": "https://api.github.com/users/gabrielinvolves/subscriptions",
        "organizations_url": "https://api.github.com/users/gabrielinvolves/orgs",
        "repos_url": "https://api.github.com/users/gabrielinvolves/repos",
        "events_url": "https://api.github.com/users/gabrielinvolves/events{/privacy}",
        "received_events_url": "https://api.github.com/users/gabrielinvolves/received_events",
        "type": "User",
        "site_admin": false
      },
      "labels": [
        {
          "id": 456432297,
          "url": "https://api.github.com/repos/involvestecnologia/agilepromoterissues/labels/ZENDESK",
          "name": "ZENDESK",
          "color": "FF0000",
          "default": false
        },
        {
          "id": 598244498,
          "url": "https://api.github.com/repos/involvestecnologia/agilepromoterissues/labels/equipe:ExD-Team",
          "name": "equipe:ExD-Team",
          "color": "fef2c0",
          "default": false
        },
        {
          "id": 456431459,
          "url": "https://api.github.com/repos/involvestecnologia/agilepromoterissues/labels/motivo:duvida",
          "name": "motivo:duvida",
          "color": "bfdadc",
          "default": false
        },
        {
          "id": 657168539,
          "url": "https://api.github.com/repos/involvestecnologia/agilepromoterissues/labels/prioridade:media",
          "name": "prioridade:media",
          "color": "e6e600",
          "default": false
        }
      ],
      "state": "open",
      "locked": false,
      "assignee": null,
      "assignees": [

      ],
      "milestone": null,
      "comments": 5,
      "created_at": "2018-03-05T20:25:22Z",
      "updated_at": "2018-03-06T14:22:04Z",
      "closed_at": null,
      "author_association": "OWNER",
      "body": "This Github issue is synchronized with Zendesk:\n\n**Ticket ID:** [#30210](https://agilepromoter.zendesk.com/agent/tickets/30210)\n**Priority:** high\n**Group:** System Never Down\n**Requester:** eder.nascimento@poptrade.com.br\n**Organization:** Pop Trade Marketing\n**Assignee:** Vinicius Rodrigues\n**Issue escalated by:** Vinicius Rodrigues\n**Produto:** produto_agile_web\n**Tipo:** solicitação\n**Url do Sistema:** \n**Ambiente:** \n**Versão Web do Cliente:** \n**Versão Android do Cliente:** \n\n\n**Original ticket description:**\n\n<blockquote><div class=\"zd-comment\">\n<p dir=\"auto\">Prezaodos, bom dia!</p>\n\n<p dir=\"auto\">A/C</p>\n\n<p dir=\"auto\">Conforme falamos, não temos a opção de zerar o mix configurado por bandeira no sistema, sendo assim, precisamos de uma ajuda com essa atividade.</p>\n\n<p dir=\"auto\">Por gentileza, verificar a possibilidade de zerar o mix diretamente no sistema, seguem dados abaixo;</p>\n\n<p dir=\"auto\">Ambiente: Wahl</p>\n\n<p dir=\"auto\">Favor verificar,\n<br>Abs.</p>\n</div></blockquote>\n\n",
      "reactions": {
        "url": "https://api.github.com/repos/involvestecnologia/agilepromoterissues/issues/3356/reactions",
        "total_count": 0,
        "+1": 0,
        "-1": 0,
        "laugh": 0,
        "hooray": 0,
        "confused": 0,
        "heart": 0
      }
    }
  ]
}
