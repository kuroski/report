import { mount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import Vuetify from 'vuetify';
import ZendeskChart from '@/components/ZendeskChart';
import issues from '../data/issuesGrouped';

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.use(Vuetify);

describe('ZendeskChart', () => {
  let props;
  let store;

  const build = () => {
    const wrapperMounted = mount(ZendeskChart, {
      store,
      localVue,
      propsData: {...props}
    });

    return {
      wrapperMounted,
    };
  };

  beforeEach(() => {
    props = { issues }
    store = new Vuex.Store({
      loading: false,
    });
  });

  it('renders ZendeskChart component correctly', () => {
    const { wrapperMounted } = build();
    expect(wrapperMounted.html()).toMatchSnapshot();
  });
});
