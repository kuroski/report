import { mount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import Vuetify from 'vuetify';
import ZendeskReasonChart from '@/components/ZendeskReasonChart';
import issues from '../data/issuesGrouped';

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.use(Vuetify);

describe('ZendeskReasonChart', () => {
  let props;
  let store;

  const build = () => {
    const wrapperMounted = mount(ZendeskReasonChart, {
      store,
      localVue,
      propsData: {...props}
    });

    return {
      wrapperMounted,
    };
  };

  beforeEach(() => {
    props = { issues }
    store = new Vuex.Store({
      loading: false,
    });
  });

  it('renders ZendeskReasonChart component correctly', () => {
    const { wrapperMounted } = build();
    expect(wrapperMounted.html()).toMatchSnapshot();
  });
});
