import nock from 'nock';
import store from 'store';
import actions from '@/store/actions';
import issues from '../data/issues';
import db from '@/db';

jest.mock('@/db');

describe('actions', () => {
  const teamLabel = 'equipe:ExD-Team';
  let dispatch;
  let commit;
  let state;

  beforeEach(() => {
    commit = jest.fn();
    dispatch = jest.fn();
    state = { teamLabel };
  });

  it('changes the team label', () => {
    const expectedTeamLabel = 'MyTeam';

    actions.CHANGE_TEAM_LABEL({ commit, dispatch }, expectedTeamLabel);

    expect(commit).toHaveBeenCalledWith('SET_TEAM_LABEL', expectedTeamLabel);
    expect(dispatch).toHaveBeenCalled();
  });

  it('fetches all github labels', async () => {
    store.set('selectedLabel', teamLabel);
    nock('https://api.github.com/repos/involvestecnologia/agilepromoterissues')
      .get('/labels')
      .query({
        per_page: 100,
      })
      .reply(200, ['domain matched']);

    await actions.FETCH_LABELS({ commit, dispatch });
    expect(commit).toHaveBeenCalledWith('SET_LABELS', ['domain matched']);
  });

  it('fetches all github default labels when no selected team', async () => {
    store.remove('selectedLabel');
    nock('https://api.github.com/repos/involvestecnologia/agilepromoterissues')
      .get('/labels')
      .query({
        per_page: 100,
      })
      .reply(200, ['domain matched']);

    await actions.FETCH_LABELS({ commit, dispatch });
    expect(commit).toHaveBeenCalledWith('SET_LABELS', ['domain matched']);
  });

  it('prevents team labels fetching when they are stored', async () => {
    const expectedLabels = ['Team1', 'Team2'];
    store.set('labels', expectedLabels);

    await actions.FETCH_LABELS({ commit, dispatch });
    expect(commit).toHaveBeenCalledWith('SET_LABELS', expectedLabels);
  });

  it('fetches all github issues', async () => {
    nock('https://api.github.com/repos/involvestecnologia/agilepromoterissues')
      .get('/issues')
      .query({
        per_page: 100,
        state: 'all',
        labels: teamLabel,
        direction: 'asc',
      })
      .reply(200, issues);

    await actions.FETCH_ISSUES({ commit, state });
    expect(commit).toHaveBeenCalledWith('SET_ISSUES', issues);
    expect(db.issues.get).toHaveBeenCalledWith(teamLabel);
    expect(db.issues.put).toHaveBeenCalledWith({
      team: teamLabel,
      data: issues
    });
  });

  it('fetches all github issues having issues in the database', async () => {
    const newIssue = [{ id: 1234 }];
    const expectedIssues = [...issues, ...newIssue];
    db.issues.get.mockReturnValue({ data: issues });

    nock('https://api.github.com/repos/involvestecnologia/agilepromoterissues')
      .get('/issues')
      .query({
        per_page: 100,
        state: 'all',
        labels: teamLabel,
        direction: 'asc',
        since: '2016-12-29 15:59:05'
      })
      .reply(200, newIssue);

    await actions.FETCH_ISSUES({ commit, state });
    expect(commit).toHaveBeenCalledWith('SET_ISSUES', expectedIssues);
    expect(db.issues.get).toHaveBeenCalledWith(teamLabel);
    expect(db.issues.put).toHaveBeenCalledWith({
      team: teamLabel,
      data: expectedIssues
    });
  });
})
