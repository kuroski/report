import mutations from '@/store/mutations';
import loadingService from '@/services/loading';

describe('mutations', () => {
  beforeEach(() => {
    loadingService.loadingAgents = 0;
  });

  it('set issues with new values', () => {
    const state = { issues: [] };
    const newIssues = [{ id: 278452527, }, { id: 278452528 }];

    mutations.SET_ISSUES(state, newIssues);
    expect(state.issues).toHaveLength(newIssues.length);
    expect(state.issues).toBe(newIssues);
  });

  it('set labels with new values', () => {
    const state = { labels: [] };
    const newLabel = [{
      color: '5319e7',
      default: false,
      id: 469209909,
      name: 'API',
      url: 'https://google.com',
     }];

    mutations.SET_LABELS(state, newLabel);
    expect(state.labels).toHaveLength(newLabel.length);
    expect(state.labels).toBe(newLabel);
  });

  it('set team label with new value', () => {
    const state = { teamLabel: '' };
    const expectedTeamLabel = 'newTeamLabel';

    mutations.SET_TEAM_LABEL(state, expectedTeamLabel);
    expect(state.teamLabel).toBe(expectedTeamLabel);
  });

  it('set loading with new value', () => {
    const state = { loading: false };

    mutations.SET_LOADING(state, true);
    expect(state.loading).toBe(true);
    expect(loadingService.loadingAgents).toBe(1);
  });

  it('increase loading stack', () => {
    const state = { loading: false };

    mutations.SET_LOADING(state, true);
    mutations.SET_LOADING(state, true);
    expect(state.loading).toBe(true);
    expect(loadingService.loadingAgents).toBe(2);
  });

  it('decrement loading stack', () => {
    const state = { loading: false };

    mutations.SET_LOADING(state, true);
    mutations.SET_LOADING(state, true);
    mutations.SET_LOADING(state, false);
    expect(state.loading).toBe(true);
    expect(loadingService.loadingAgents).toBe(1);
  });

  it('set loading state to false when stack is empty', () => {
    const state = { loading: false };

    mutations.SET_LOADING(state, true);
    mutations.SET_LOADING(state, true);
    mutations.SET_LOADING(state, false);
    mutations.SET_LOADING(state, false);
    expect(state.loading).toBe(false);
    expect(loadingService.loadingAgents).toBe(0);
  });
});
