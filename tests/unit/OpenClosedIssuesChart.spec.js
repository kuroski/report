import { mount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import Vuetify from 'vuetify';
import OpenClosedIssuesChart from '@/components/OpenClosedIssuesChart';
import issues from '../data/issuesGrouped';

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.use(Vuetify);

describe('OpenClosedIssuesChart', () => {
  let props;
  let store;

  const build = () => {
    const wrapperMounted = mount(OpenClosedIssuesChart, {
      store,
      localVue,
      propsData: {...props}
    });

    return {
      wrapperMounted,
    };
  };

  beforeEach(() => {
    props = { issues }
    store = new Vuex.Store({
      loading: false,
    });
  });

  it('renders OpenClosedIssuesChart component correctly', () => {
    const { wrapperMounted } = build();
    expect(wrapperMounted.html()).toMatchSnapshot();
  });
});
