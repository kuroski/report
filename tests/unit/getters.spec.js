import getters from '@/store/getters';
import issues from '../data/issues';

describe('getters', () => {
  const teamLabel = 'equipe:ExD-Team';
  let state;

  beforeEach(() => {
    state = { issues, teamLabel, labels: [{ name: teamLabel }] };
  });

  it('lists all available issues', () => {
    const expectedIssues = getters.availableIssues(state)();
    expect(expectedIssues).toEqual(issues);
  });

  it('lists all available issues filtered by labels', () => {
    const expectedIssues = getters.availableIssues(state)(teamLabel);
    expect(expectedIssues).not.toEqual(issues);
    expect(expectedIssues).toHaveLength(issues.length - 1);
  });

  it('lists all team labels', () => {
    const expectedLabels = getters.teamLabels(state)();
    expect(expectedLabels).toEqual([teamLabel]);
  });
})
