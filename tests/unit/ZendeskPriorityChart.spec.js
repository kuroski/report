import { mount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import Vuetify from 'vuetify';
import ZendeskPriorityChart from '@/components/ZendeskPriorityChart';
import issues from '../data/issuesGrouped';

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.use(Vuetify);

describe('ZendeskPriorityChart', () => {
  let props;
  let store;

  const build = () => {
    const wrapperMounted = mount(ZendeskPriorityChart, {
      store,
      localVue,
      propsData: {...props}
    });

    return {
      wrapperMounted,
    };
  };

  beforeEach(() => {
    props = { issues }
    store = new Vuex.Store({
      loading: false,
    });
  });

  it('renders ZendeskPriorityChart component correctly', () => {
    const { wrapperMounted } = build();
    expect(wrapperMounted.html()).toMatchSnapshot();
  });
});
