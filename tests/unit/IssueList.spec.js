import { DateTime } from 'luxon';
import { shallowMount, mount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import Vuetify from 'vuetify';
import IssueList from '@/components/IssueList';
import issues from '../data/issues';

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.use(Vuetify);

describe('IssueList', () => {
  let props;

  const build = () => {
    const store = new Vuex.Store({
      loading: false,
    });

    const app = document.createElement('div');
    app.setAttribute('data-app', true);
    document.body.append(app);

    const wrapperMounted = mount(IssueList, {
      store,
      localVue,
      propsData: {...props}
    });

    const firstIssue = issues[0];

    return {
      wrapperMounted,
      firstIssue,
    };
  };

  beforeEach(() => {
    props = { issues }
  });

  it('renders IssueList component correctly', () => {
    const { wrapperMounted } = build()
    expect(wrapperMounted.html()).toMatchSnapshot();
  });

  it('have an issues list', () => {
    const { wrapperMounted } = build();
    expect(wrapperMounted.findAll('tbody > tr')).toHaveLength(issues.length);
  });

  it('shows an message when list is empty', () => {
    props.issues = [];
    const { wrapperMounted } = build();
    expect(wrapperMounted.find('.list-empty').isVisible()).toBe(true);
    expect(wrapperMounted.vm.showAlert).toBe(true);
  });

  it('hides empty list message when list have items', () => {
    const { wrapperMounted } = build();
    expect(wrapperMounted.find('.list-empty').exists()).toBe(false);
    expect(wrapperMounted.vm.showAlert).toBe(false);
  });

  it('shows issue number', () => {
    const { wrapperMounted, firstIssue } = build();
    expect(parseInt(wrapperMounted.find('tbody > tr > td:nth-child(1) > a').text())).toBe(firstIssue.number);
  });

  it('shows issue title', () => {
    const { wrapperMounted, firstIssue } = build();
    expect(wrapperMounted.find('tbody > tr > td:nth-child(2)').text()).toBe(firstIssue.title);
  });

  it('shows issue status', () => {
    const { wrapperMounted, firstIssue } = build();
    expect(wrapperMounted.find('tbody > tr > td:nth-child(3)').text()).toBe(firstIssue.state);
  });

  it('shows issue labels', () => {
    const { wrapperMounted, firstIssue } = build();
    const labelsFirstContainer = wrapperMounted.find('tbody > tr > td:nth-child(4)');
    const expectedLabelsLength = firstIssue.labels.length;
    expect(labelsFirstContainer.findAll('.chip')).toHaveLength(expectedLabelsLength);
  });

  it('shows issue created at', () => {
    const { wrapperMounted, firstIssue } = build();
    expect(wrapperMounted.find('tbody > tr > td:nth-child(5)').text()).toBe(DateTime.fromISO(firstIssue.created_at).toLocaleString());
  });
});
